'use strict';

module.exports = {
  url: 'https://nomce.me',
  pathPrefix: '/',
  title: 'Blog by Nomce',
  subtitle: 'Test/Application Consultant and a fun guy :)',
  copyright: '© All rights reserved.',
  disqusShortname: '',
  postsPerPage: 4,
  googleAnalyticsId: 'UA-73379983-2',
  useKatex: false,
  menu: [
    {
      label: 'About me',
      path: '/pages/about'
    },
    {
      label: 'Projects',
      path: '/pages/projects'
    },
    {
      label: 'Contact me',
      path: '/pages/contacts'
    }
  ],
  author: {
    name: 'Naum Spaseski',
    photo: '/photo.jpg',
    bio: 'Test/Application Consultant and a fun guy :)',
    contacts: {
      email: 'nomce@nomce.me',
      facebook: '',
      telegram: '',
      twitter: 'nomce',
      github: 'nomce',
      rss: '',
      vkontakte: '',
      linkedin: 'naum.spaseski',
      instagram: '',
      line: '',
      gitlab: '',
      weibo: '',
      codepen: '',
      youtube: '',
      soundcloud: '',
    }
  }
};
