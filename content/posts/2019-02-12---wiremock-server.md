---
title: Mocking data with Wiremock
date: "2019-02-12T23:46:37.121Z"
template: "post"
draft: false
slug: "mocking-data-with-wiremock"
category: New Tools"
tags:
  - "Mocking"
description: "I've heard my collegues talking about it, and decided to try it by myself and see what it is. So: Wiremock, simple but powerful mock server."
socialImage: "/media/image-2.jpg"
---

I've heard my collegues talking about it, and decided to try it by myself and see what it is. So: [Wiremock](http://wiremock.org). A tool that helps adding fake data to your project in order to facilitate your testing and/or debugging. If you don't know what mock is, here is [the wikipedia article](https://en.wikipedia.org/wiki/Mock_object).

This tool is a simple HTTP(S) Java server that can replicate data staticaly using json files to describe them. It it very simple but powerful. Simple, because one can control it with JSON files with ready matching HTTP requests and canny responses mmm... to respond with. Or you can add more grainy control with conditions for what to match (different headers, bodies with either raw text, JSON or XML etc.) and flexibility for dynamic responses with variables contitions etc. If you want to go one level up, you can integrate it using Maven with your existing project and configure the responses programatically (in Java directly, instead of using JSON). It can integrate well with Spring too.

To begin our example, download the latest Wiremock standalone and launch it with standard parameters (port 8080). The url to access the server will be: http://localhost:8080, and to see all mocked files http://localhost:8080/__admin. If you want to add your requests/responses you can put all as JSON files inside the mappings folder next to the Wiremock .jar file. The [stubbing](http://wiremock.org/docs/stubbing/) documentation explains in simple terms how to construct your JSON files - roughly, you need a request object with method and URL (and headers if you need something special to match for), and response object with the status code, the body of the message, and eventually some headers to send back.

One fantastic feature that Wiremock has is the Recorder. Let's say you want your web site project to include some data from Wikipedia. You will get all Wikipedia data from this URL: "https://en.wikipedia.org/w/api.php" (if I've inspired you, here is [the full Wikipedia API docs](https://www.mediawiki.org/wiki/API:Main_page)). So, you open the Recorder at http://localhost:8080/__admin/recorder, put the Wikipedia domain ("https://en.wikipedia.org") in the field and click Record. Now, everytime that you send a request to the Wiremock instance, Wiremock will receive it, will redirect it to Wikipedia, Wikipedia will respond, Wiremock will save the request/response pair in the memory, and for the sake of completeness, it will return the real response to you (so you don't feel mocked). Once you stop the recorder, it will write all data to the mappings folder for future reuse.

Now, if you open the admin page ("http://localhost:8080/__admin"), you will see all requests you have made during the recording session and the responses you have received. If you retry the same requests after you stopped the recording, you will get the same responses from Wiremock, but with a significant difference, they will be served from the "mappings" folder, instead of the Wikipedia API. Thus, right now you have obtained two things: control over the data (it won't change involuntarily) and less load of the Wikipedia server (which is also important). More important, you can use this data to build automatic tests, develop your project without disruption and be more confident when you have to test the project end-to-end with real data or even in production!

Cheers!
