---
title: "About me"
template: "page"
socialImage: "/media/image-2.jpg"
---

Hi, I’m Naum Spaseski. I’m a Test/Application Consultant for [Sogeti France](https://www.fr.sogeti.com/). 
As such, right now I’m at [Air France](https://airfrance.com), where I work with different testing technologies. 
Previously I was at [EGM](https://eglobalmark.com), a startup in Sophia Antipolis, France, working on the international standard [oneM2M](https://oneM2M.org).
I was part of different STFs at the [European Telecommuncations Standardisation Institute (ETSI)](https://etsi.org). 
I’m an ISTQB Certified Tester. If you need it, my complete CV is on LinkedIn.

In my free time I play guitar, experiment with some new technologies, travel, and write this blog. 
I love photography. Hey, I graduated on Friday, 13 June :)
