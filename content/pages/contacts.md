---
title: "Contact me"
template: "page"
socialImage: "/media/image-4.jpg"
---

You can email me or find me on Twitter (links on the left) if you want to get in touch. I love meeting new people so don't hesitate to send a message!

<form action="https://formspree.io/nomce@nomce.me" method="POST" class="form" id="contact-form">      
  <input name="name" type="text" class="feedback-input" placeholder="Name" />   
  <input name="email" type="text" class="feedback-input" placeholder="Email" />
  <textarea name="text" class="feedback-input" placeholder="Comment"></textarea>
  <input type="text" name="_gotcha" style="display:none">
  <input type="submit" value="Submit"/>
  <input type="hidden" name="_next" value="./aboutme?message=Your message was sent successfully, thanks!" />  
</form>
