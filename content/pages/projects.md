---
title: "About me"
template: "page"
socialImage: "/media/image-2.jpg"
---

These are some of the things I've made and done:

* [Eclipse IoT-Testware](https://projects.eclipse.org/projects/technology.iottestware) is a project to provide a rich set of TTCN-3 test suites and test cases for IoT technologies to enable developers in setting up a comprehensive test environment of their own.
* [ARMOUR H2020 Project](https://www.armour-project.eu/) is a project to provide duly tested, benchmarked and certified Security & Trust solutions for large-scale IoT using upgraded FIRE large-scale IoT/Cloud testbeds. I worked on generating TTCN-3 test cases from UML models.
* [Karti](https://karti.nomce.me/) is a webpage that I created because my friends and I, while on the beach in our sunny hometown Struga, were constantly searching for a pen and paper to note the results of the card games that we played there. So, the result is this simple web app, written in Bootstrap 3 and jQuery. The code is publicly available on [Gitlab](https://gitlab.com/karti/karti.gitlab.io), but not maintained.

Also, here are some of my publications:

* [Continuous integration with mock data in high level testing](/documents/2019-10-Presentation_UCAAT_Naum_Spaseski.pdf), Naum Spaseski, ETSI UCAAT, October 2019 ([PPT format](/documents/2019-10-Presentation_UCAAT_Naum_Spaseski.ppt))
* [Testing Security in OneM2M](/documents/2017-09-ConnectSecurityWorld-EGM_Naum_Spaseski.pdf), Naum Spaseski, Connect Security World, September 2017
* [A Survey on Model-Based Testing Tools for Test Case Generation](https://books.google.fr/books?id=ReJEDwAAQBAJ&pg=PA77&lpg=PA77&dq=naum+spaseski&source=bl&ots=VeEwQsRFIC&sig=ACfU3U3mUQ85j05ozkvGWDgOsYbHzLCIeA&hl=en&sa=X&ved=2ahUKEwjgwunome3pAhUQHxoKHehRCrUQ6AEwEHoECAoQAQ#v=onepage&q=naum%20spaseski&f=false), Wenbin Li, Franck Le Gall, Naum Spaseski, Tools & Methods of Program Analysis International Conference (TMPA-2017), March 2017
* [MBT to TTCN-3 tool chain: The OneM2M experience](/documents/2016-10-Presentation-MBT-to-TTCN3-tool-chain-the-oneM2M-experience.pdf), Abbas AHMAD, Elizabeta FOURNERET, Bruno LEGEARD, Franck LE GALL, Elemér Lelik, György Réthy, Naum SPASESKI, ETSI UCAAT, October 2016
* [Development of an Open-Sourced Conformance Testing Tool – oneM2M IoT Tester](/documents/2016-10-Presentation-Testing-OneM2M-compliant-implementations.pdf), oneM2M Tester Team: Ting Miao, Nak-Myoung Sung, JaeYoung Hwang, Naum Spaseski, György Réthy, Elemer Lelik, JaeSeung Song, Jaeho Kim, ETSI UCAAT, October 2016
